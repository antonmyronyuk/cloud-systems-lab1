FROM python:3.10-slim

RUN apt-get update -y && apt-get install -y git gcc curl

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install --upgrade pip  && pip install --no-cache-dir -r requirements.txt

COPY . .
