build:
	docker compose build --parallel

up:
	docker compose up anton-myroniuk-ip31mn-app

down:
	docker compose down --remove-orphans
