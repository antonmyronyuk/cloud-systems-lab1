import os

import sqlalchemy as sa
from flask import Flask, render_template_string
from redis import Redis

app = Flask(__name__)

engine = sa.create_engine((
    f'postgresql://{os.getenv("POSTGRES_USER")}:{os.getenv("POSTGRES_PASSWORD")}@'
    f'{os.getenv("POSTGRES_HOST")}/{os.getenv("POSTGRES_DB")}'
))
redis = Redis(host=os.getenv("REDIS_HOST"), port=os.getenv("REDIS_PORT"))


@app.route("/")
def index():
    page_visited_count = redis.incrby("cars:visit:counter", 1)

    with engine.connect() as connection:
        cars = connection.execute(sa.text("SELECT id, name, year FROM cars")).fetchall()

    return render_template_string(
        """<h2>Page visited: {{ page_visited_count }}</h2>
        Cars:
        <ul>
        {% for id, name, year in cars %}
            <li>ID: {{ id }}, name: {{ name }}, year: {{ year }}</li>
        {% endfor %}
        </ul>""",
        page_visited_count=page_visited_count,
        cars=cars,
    )


@app.route("/hello")
def hello():
    return """
        <h1>Hello from Anton Myroniuk (IP-31mn)</h1>
        <img src="/static/rick.jpeg">
    """
